using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Monsters;
using Effects;
using DroppedItems;

public class Player : MonoBehaviour
{
    [Range(0, 10)]
    public float DamageModificator = 0;

    [SerializeField] private Camera _camera;
    [SerializeField] private float _damage;
    [SerializeField] private HitEffect _hitEffect;

    private void Update()
    {
        makeHit();
    }

    private void makeHit()
    {
        if (!Input.GetMouseButtonDown(0)) return;
        if (EventSystem.current.IsPointerOverGameObject()) return;
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider == null) return;

        Monster hittedMonster;
        if(hit.collider.TryGetComponent<Monster>(out hittedMonster))
        {
            hittedMonster.Damage(getDamageToApply());
            instantiateHitEffect(hit.point);
        }

        DroppedItem hittedDroppedBooster;
        if (hit.collider.TryGetComponent<DroppedItem>(out hittedDroppedBooster))
        {
            hittedDroppedBooster.Pick();
        }
    }
    private HitEffect instantiateHitEffect(Vector2 pos)
    {
        HitEffect instance = Instantiate(_hitEffect);
        instance.transform.position = pos;
        instance.PlayEffect();
        instance.SetTextDamageCount(getDamageToApply());

        return instance;
    }
    private int getDamageToApply()
    {
        return Mathf.FloorToInt(_damage * DamageModificator);
    }
}

