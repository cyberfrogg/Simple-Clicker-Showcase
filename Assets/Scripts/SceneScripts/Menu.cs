using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SceneScripts
{
    public class Menu : MonoBehaviour
    {
        public void LoadGameScene()
        {
            SceneManager.LoadScene("Game");
        }
        public void ExitGame()
        {
            Application.Quit();
        }
    }
}