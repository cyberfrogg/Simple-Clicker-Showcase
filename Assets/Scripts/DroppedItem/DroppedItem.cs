using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DroppedItems
{
    public abstract class DroppedItem : MonoBehaviour
    {
        public virtual void Pick()
        {

        }
    }
}