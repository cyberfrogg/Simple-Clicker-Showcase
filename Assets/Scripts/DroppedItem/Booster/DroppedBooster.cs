using Boosters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DroppedItems
{
    public class DroppedBooster : DroppedItem
    {
        public Booster Booster;
        [SerializeField] private SpriteRenderer _icon;

        private void Start()
        {
            _icon.sprite = Booster.Icon;
        }

        public override void Pick()
        {
            base.Pick();

            Game.Instance.BoosterHub.AddBosster(Booster);

            Destroy(gameObject);
        }
    }
}