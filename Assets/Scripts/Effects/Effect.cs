using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Effects
{
    public abstract class Effect : MonoBehaviour
    {
        public UnityEvent OnAnimationEnd;

        [SerializeField] private float _durationLifetime;

        public virtual void PlayEffect()
        {
            Debug.Log($"Playing effect: {gameObject.name}");
            StartCoroutine(makeDuration(_durationLifetime));
        }
        public virtual void EndLifeCycle()
        {
            Debug.Log($"Effect stopped: {gameObject.name}");
            OnAnimationEnd.Invoke();
            Destroy(gameObject);
            OnAnimationEnd.RemoveAllListeners();
        }

        
        private IEnumerator makeDuration(float duration)
        {
            yield return new WaitForSeconds(duration);

            EndLifeCycle();
        }
    }
}