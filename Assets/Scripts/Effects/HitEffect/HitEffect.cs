using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Effects
{
    public class HitEffect : Effect
    {
        [SerializeField] private TMP_Text[] _damageTexts;

        public void SetTextDamageCount(int damage)
        {
            foreach (TMP_Text text in _damageTexts)
            {
                text.text = $"-{damage}";
            }
        }
    }
}