using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Effects
{
    public class DeathEffect : Effect
    {
        [SerializeField] private Vector3 _spawnOffset;

        public override void PlayEffect()
        {
            base.PlayEffect();

            transform.localPosition = transform.localPosition + _spawnOffset;
        }
    }
}