﻿using DroppedItems;
using Effects;
using UnityEngine;
using UnityEngine.Events;

namespace Monsters
{
    public abstract class Monster : MonoBehaviour
    {
        public int Health
        {
            get
            {
                return _health;
            }
            set
            {
                _health = value;
                _health = Mathf.Clamp(_health, 0, MaxHealth);
                OnDamage(value);
                OnDamageEvent.Invoke();

                if(_health <= 0)
                {
                    OnDeath();
                    OnDeathEvent.Invoke();
                }
            }
        }
        public UnityEvent OnDamageEvent;
        public UnityEvent OnDeathEvent;
        public int MaxHealth;

        [SerializeField] private int _health;
        [Range(10, 1000)] [SerializeField] private int _scoreModifier = 1;
        [SerializeField] private Animator _hitAnimator;
        [SerializeField] private Effect _deathEffect;
        [Space]
        [Range(0, 100)]
        [SerializeField] private int _dropItemChance;
        [SerializeField] private DroppedItem[] _droppedItems;

        private void Awake()
        {
            
            _health = MaxHealth;
        }

        public virtual void Damage(int damage)
        {
            Health -= damage;
        }
        public virtual void DropItem(DroppedItem item)
        {
            DroppedItem itemInstance = Instantiate(item);
            itemInstance.transform.position = transform.position;
        }

        protected virtual void OnDamage(int damage)
        {
            Debug.Log($"{gameObject.name} damaged for: {damage} poins. Now - {Health}");
            _hitAnimator.Play("monsterHit_animation");
        }
        protected virtual void OnDeath()
        {
            Debug.Log($"{gameObject.name} died!");

            Effect deathEffectInstance = Instantiate(_deathEffect);
            deathEffectInstance.transform.position = transform.position;
            deathEffectInstance.PlayEffect();

            Game.Instance.Statistics.Kills += 1;
            Game.Instance.Statistics.Score += 1 * _scoreModifier;

            if (Random.Range(0, _dropItemChance) == 1)
            {
                DropItem(_droppedItems[Random.Range(0, _droppedItems.Length)]);
            }

            Destroy(gameObject);
        }
        
    }
}
