using Effects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Monsters
{
    public class MonsterSpawnPoint : MonoBehaviour
    {
        public Monster MonsterOnPoint { get; private set; }

        [SerializeField] private Effect _spawnEffect;
        [SerializeField] private MonsterSpawner _spawner;
        private Monster _monsterToSpawn;

        public void SpawnMonster(Monster monster)
        {
            _monsterToSpawn = monster;

            Effect effectInstance = Instantiate(_spawnEffect);
            effectInstance.transform.position = transform.position;
            effectInstance.PlayEffect();
            effectInstance.OnAnimationEnd.AddListener(spawnMonster);
        }
        private void spawnMonster()
        {
            Monster monsterInstance = Instantiate(_monsterToSpawn);
            monsterInstance.transform.position = transform.position;
            monsterInstance.MaxHealth += monsterInstance.MaxHealth * Game.Instance.Statistics.Level;
            monsterInstance.Health += monsterInstance.Health * Game.Instance.Statistics.Level;
            MonsterOnPoint = monsterInstance;
        }
    }
}