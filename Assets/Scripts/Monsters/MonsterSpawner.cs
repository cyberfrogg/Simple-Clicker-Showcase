using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Monsters;
using System.Linq;

public class MonsterSpawner : MonoBehaviour
{
    public int CurrentTierIndex { get; private set; }
    
    [SerializeField] private MonsterSpawningTier[] _tiers;
    [SerializeField] private MonsterSpawnPoint[] _spawnPoints;
    [SerializeField] private float _spawnPeriod = 10f;
    private float _nextSpawnTime;

    private void Update()
    {
        if (Time.time > _nextSpawnTime)
        {
            _nextSpawnTime += _spawnPeriod;

            spawnNextMonster();
        }
    }
    private void spawnNextMonster()
    {
        MonsterSpawningTier currentTier = _tiers[Mathf.Clamp(CurrentTierIndex, 0, _tiers.Length)];

        MonsterSpawnPoint[] availablePoints = _spawnPoints.Where(x => x.MonsterOnPoint == null).ToArray();

        if (availablePoints.Length == 0)
        {
            Debug.Log("No available points to spawn!");
            Game.Instance.OnPlayerDeath.Invoke();
            return;
        }

        availablePoints[0].SpawnMonster(currentTier.RandomMonster);

        Debug.Log(CurrentTierIndex + " - " + _tiers.Length);
        if(CurrentTierIndex < _tiers.Length)
        {
            if (Game.Instance.Statistics.Score >= _tiers[CurrentTierIndex + 1].ScoreToActivate)
            {
                CurrentTierIndex++;
                Game.Instance.Statistics.Level = CurrentTierIndex;
            }
        }
    }
}
