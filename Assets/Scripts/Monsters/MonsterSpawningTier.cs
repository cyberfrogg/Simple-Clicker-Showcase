﻿using System;
using UnityEngine;

namespace Monsters
{
    [Serializable]
    public class MonsterSpawningTier
    {
        public int ScoreToActivate;
        [Range(0, 50)] public float ProgressionModifier;
        public Monster RandomMonster
        {
            get
            {
                return _monsters[UnityEngine.Random.Range(0, _monsters.Length)];
            }
        }

        [SerializeField] private Monster[] _monsters;
    }
}
