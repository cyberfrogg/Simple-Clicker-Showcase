using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boosters
{
    public class BoosterHub : MonoBehaviour
    {
        [SerializeField] private BoosterDisplay _displayPrefab;
        [SerializeField] private Transform _displaysConainer;
        private List<Booster> _boosters = new List<Booster>();

        [SerializeField] private Booster _sampleBooster;

        public void AddBosster(Booster booster)
        {
            Booster instantiatedBooster = Instantiate(booster);

            _boosters.Add(instantiatedBooster);
            instantiatedBooster.ActivateBooster();
            displayBooster(instantiatedBooster);
        }
        public void AddSampleBooster()
        {
            AddBosster(_sampleBooster);
        }

        private BoosterDisplay displayBooster(Booster booster)
        {
            BoosterDisplay display = Instantiate(_displayPrefab);
            display.transform.SetParent(_displaysConainer, false);
            display.Init(booster);

            return display;
        }

        private void Update()
        {
            cleanUpBoosterList();
        }
        private void cleanUpBoosterList()
        {
            for (int i = 0; i < _boosters.Count; i++)
            {
                if (_boosters[i] == null)
                {
                    _boosters.RemoveAt(i);
                    break;
                }
            }
        }
    }
}