﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Boosters
{
    public class StrengthBooster : Booster
    {
        [SerializeField] private float _damageModifier;
        private Player _currentPlayer;

        public override void ActivateBooster()
        {
            base.ActivateBooster();
            _currentPlayer = GameObject.FindObjectOfType<Player>();
            if (_currentPlayer == null) return;

            _currentPlayer.DamageModificator += _damageModifier;
        }
        public override void EndBooster()
        {
            base.EndBooster();
            if (_currentPlayer == null) return;

            _currentPlayer.DamageModificator -= _damageModifier;
        }
    }
}
