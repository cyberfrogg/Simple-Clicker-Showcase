﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Boosters
{
    public abstract class Booster : MonoBehaviour, ICloneable
    {
        public string Name;
        public Sprite Icon;
        [Range(0, 100f)]
        public float Duration = 3f;
        public float CurrentTime;
        public bool IsAlive;
        public UnityEvent OnBoosterEnd;

        public object Clone()
        {
            return this.MemberwiseClone();
        }
        public virtual void ActivateBooster()
        {
            CurrentTime = Duration;
            Debug.Log($"Booster activated: {Name}");
            IsAlive = true;
        }
        public virtual void EndBooster()
        {
            Debug.Log($"Booster end: {Name}");

            OnBoosterEnd.Invoke();
            Destroy(gameObject);
        }

        public virtual void Update()
        {
            if(CurrentTime > 0)
            {
                CurrentTime -= Time.deltaTime;
            }
            else
            {
                EndBooster();
            }
        }
    }
}
