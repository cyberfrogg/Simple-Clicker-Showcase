using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Boosters
{
    public class BoosterDisplay : MonoBehaviour
    {
        [SerializeField] private Image _icon;
        [SerializeField] private Image _durationFillImage;

        private Booster _currentBooster;

        public void Init(Booster booster)
        {
            _currentBooster = booster;
            _currentBooster.OnBoosterEnd.AddListener(removeSelf);
        }

        private void Update()
        {
            if (_currentBooster == null) return;

            _icon.sprite = _currentBooster.Icon;
            _durationFillImage.fillAmount = _currentBooster.CurrentTime / _currentBooster.Duration;
        }
        private void removeSelf()
        {
            Destroy(gameObject);
        }
    }
}