using Monsters;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Statistics
{
    public class InGameStatistics : MonoBehaviour
    {
        [Header("Texts:")]
        [SerializeField] private TMP_Text[] _killsText;
        [SerializeField] private TMP_Text[] _scoreText;
        [SerializeField] private TMP_Text[] _levelText;
        public int Kills
        {
            get
            {
                return _kills;
            }
            set
            {
                _kills = value;
                redrawStatistics();
            }
        }
        public int Score
        {
            get
            {
                return _score;
            }
            set
            {
                _score = value;
                redrawStatistics();
            }
        }
        public int Level
        {
            get
            {
                return _level;
            }
            set
            {
                _level = value;
                redrawStatistics();
            }
        }

        private int _kills;
        private int _score;
        private int _level;

        private void redrawStatistics()
        {
            foreach (TMP_Text text in _killsText)
            {
                text.text = Kills.ToString("000000");
            }
            foreach (TMP_Text text in _scoreText)
            {
                text.text = Score.ToString("000000");
            }
            foreach (TMP_Text text in _levelText)
            {
                text.text = $"Level: {Level.ToString("00")}";
            }
        }
    }
}