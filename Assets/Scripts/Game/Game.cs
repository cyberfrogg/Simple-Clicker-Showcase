using Boosters;
using Statistics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Game : MonoBehaviour
{
	public static Game Instance;

	public MonsterSpawner MonsterSpawner
    {
		get { return _monsterSpawner; }
		private set { _monsterSpawner = value; }
	}
	public BoosterHub BoosterHub
	{
		get { return _boosterHub; }
		private set { _boosterHub = value; }
	}
	public InGameStatistics Statistics
    {
		get { return _statistics; }
		private set { _statistics = value; }
	}
	public GameState GameState { get; private set; }
	public UnityEvent OnPlayerDeath;

	[SerializeField] private BoosterHub _boosterHub;
	[SerializeField] private MonsterSpawner _monsterSpawner;
	[SerializeField] private InGameStatistics _statistics;

    private void Start()
	{
		if (Instance == null)
			Instance = this;
		else if (Instance == this)
			Destroy(gameObject);


		OnPlayerDeath.AddListener(playerDeathAction);
	}

	private void playerDeathAction()
    {
		GameState = GameState.GameOver;
    }
}
